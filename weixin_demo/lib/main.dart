import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'pages/WechatPage.dart';
import 'pages/DiscoveryPage.dart';
import 'pages/ContactListPage.dart';
import 'pages/PersonPage.dart';
import 'package:fluro/fluro.dart';
import 'route/Routes.dart';
import 'route/Application.dart';

void main() {
  runApp(MyApp());
}
// void main() {
//   runApp(MultiProvider(
//     providers: [],
//     child: MyApp(),
//   ));
// }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final router = FluroRouter();
    Routes.configureRoutes(router);
    Application.router = router;

    return ScreenUtilInit(
        designSize: Size(750, 1334), // 设计稿中设备的尺寸(单位随意,但在使用过程中必须保持一致)
        allowFontScaling: false, // 设置字体大小是否根据系统的“字体大小”辅助选项来进行缩放
        child: MaterialApp(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          onGenerateRoute: Application.router.generator,
          initialRoute: "/",
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
            splashColor: Colors.green, // 统一控制水波纹
            backgroundColor:
                Colors.grey[300], // 与主色形成对比的颜色，例如用作进度条的剩余部分。暂时用作背景色
            dividerColor: Colors.grey[300],
            appBarTheme: AppBarTheme(
              color: Colors.grey[300],
              elevation: 0.0,
              textTheme: TextTheme(
                  headline6: TextStyle(
                color: Colors.black,
                fontSize: 18,
              )),
            ),
          ),
          home: MyHomePage(title: 'weixin'),
        ));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<BottomNavigationBarItem> bottomNavItems = [
    BottomNavigationBarItem(
        // backgroundColor: Colors.blue,
        icon: Icon(CupertinoIcons.home),
        // title: Text("首页"),
        label: "微信"),
    BottomNavigationBarItem(
        icon: Icon(CupertinoIcons.person_2),
        // backgroundColor: Colors.green,
        // title: Text("消息"),
        label: "通讯录"),
    BottomNavigationBarItem(
        // backgroundColor: Colors.purple,
        icon: Icon(CupertinoIcons.sort_up),
        // title: Text("消息"),
        label: "发现"),
    BottomNavigationBarItem(
        // backgroundColor: Colors.red,
        icon: Icon(CupertinoIcons.person),
        // title: Text("个人中心"),
        label: "个人中心"),
  ];

  int currentIndex;

  final pages = [
    WechatPage(),
    ContactListPage(),
    DiscoveryPage(),
    PersonPage()
  ];

  @override
  void initState() {
    super.initState();
    currentIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(widget.title),
      //   centerTitle: true,
      // ),
      bottomNavigationBar: BottomNavigationBar(
        items: bottomNavItems,
        currentIndex: currentIndex,
        type: BottomNavigationBarType.fixed,
        selectedIconTheme: IconThemeData(color: Colors.green, size: 35),
        onTap: (index) {
          _changePage(index);
        },
      ),
      body: pages[currentIndex],
    );
  }

  /*切换页面*/
  void _changePage(int index) {
    /*如果点击的导航项不是当前项  切换 */
    if (index != currentIndex) {
      setState(() {
        currentIndex = index;
      });
    }
  }
}
