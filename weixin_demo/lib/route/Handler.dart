import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:weixin_demo/pages/PersonPage.dart';
import '../pages/WechatPage.dart';

//indexpage
Handler homePageHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> paramMap) {
  // String title = 'homepage';
  return WechatPage();
});
Handler personPageHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> paramMap) {
  // String title = 'homepage';
  return PersonPage();
});
