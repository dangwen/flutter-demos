import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:weixin_demo/route/Handler.dart';

class Routes {
  static String root = '/';
  static String homePage = 'homePage';
  static String personPage = 'personPage';
  // static String adoptPage = 'adoptPage';
  // static String userSetting = 'userSetting';
  // static String adoptTicket = 'adoptTicket';
  // static String totalIncome = 'totalIncome';
  // static String totalAssets = 'totalAssets';
  // static String recommendIncome = 'recommendIncome';

  static void configureRoutes(FluroRouter router) {
    router.notFoundHandler = new Handler(handlerFunc:
        (BuildContext context, Map<String, List<String>> paramMap) {
      print('wenwen page not found');
      return;
    });

    router.define(root, handler: homePageHandler);
    router.define(homePage, handler: homePageHandler);
    router.define(personPage, handler: personPageHandler);
    // router.define(userSetting, handler: userSettingPageHandler);
    // router.define(adoptTicket, handler: adoptTicketPageHandler);
    // router.define(totalIncome, handler: totalIncomePageHandler);
    // router.define(totalAssets, handler: totalAssetsPageHandler);
    // router.define(recommendIncome, handler: recommendIncomePageHandler);
    // router.define(homePage,handler: homePageHandler);
  }
}
