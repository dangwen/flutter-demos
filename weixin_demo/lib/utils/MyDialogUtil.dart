import 'package:flutter/material.dart';

class MyDialogUtil {
  showAlertDialog(BuildContext context, String title, String content,
      String buttonText1, String buttonText2) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(content),
            actions: [
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(buttonText1)),
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(buttonText2)),
            ],
          );
        });
  }
}
