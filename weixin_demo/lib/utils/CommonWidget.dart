import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weixin_demo/utils/CommonColor.dart';
import 'package:weixin_demo/utils/MyDialogUtil.dart';

class CommonWidget {
  Text listTileText(String text) {
    return Text(text);
  }

  Widget singleListile(BuildContext context, String titleText,
      IconData iconData, Function onTap) {
    return Container(
      alignment: Alignment.center,
      height: ScreenUtil().setHeight(100),
      width: ScreenUtil().setWidth(750),
      decoration: BoxDecoration(
          color: CommonColor().whiteColor(),
          border: Border(bottom: BorderSide(color: Colors.grey[300]))),
      child: ListTile(
        leading: Icon(iconData),
        title: listTileText(titleText),
        trailing: Icon(CupertinoIcons.right_chevron),
        onTap: () {
          print(1);
          MyDialogUtil().showAlertDialog(context, "提示", "暂无内容", "确定", "取消");
        },
      ),
    );
  }

  Widget singleListile2(BuildContext context, String titleText,
      IconData iconData, Function onTap) {
    return Container(
      alignment: Alignment.center,
      height: ScreenUtil().setHeight(100),
      width: ScreenUtil().setWidth(750),
      decoration: BoxDecoration(
          color: CommonColor().whiteColor(),
          border: Border(bottom: BorderSide(color: Colors.grey[300]))),
      child: ListTile(
        leading: Icon(iconData),
        title: listTileText(titleText),
        onTap: () {
          print(1);
          MyDialogUtil().showAlertDialog(context, "提示", "暂无内容", "确定", "取消");
        },
      ),
    );
  }

  Widget listView(BuildContext context, {var size = 1}) {
    return Container(
      alignment: Alignment.center,
      height: ScreenUtil().setHeight(400),
      width: ScreenUtil().setWidth(750),
      color: CommonColor().whiteColor(),
      child: ListView(
        children: [
          singleListile(context, "收藏", Icons.collections, () {}),
          singleListile(context, "相册", Icons.picture_in_picture, () {}),
          singleListile(context, "卡包", Icons.credit_card, () {}),
          singleListile(context, "表情", Icons.emoji_emotions, () {}),
        ],
      ),
    );
  }

  Widget listView2(BuildContext context, {var size = 1}) {
    return Container(
      alignment: Alignment.center,
      height: ScreenUtil().setHeight(500),
      width: ScreenUtil().setWidth(750),
      color: CommonColor().whiteColor(),
      child: ListView(
        children: [
          singleListile2(context, "新的朋友", Icons.person_add, () {}),
          singleListile2(context, "仅聊天的朋友", Icons.person_pin, () {}),
          singleListile2(context, "群聊", CupertinoIcons.person_2, () {}),
          singleListile2(context, "标签", Icons.tag, () {}),
          singleListile2(context, "公众号", CupertinoIcons.person, () {}),
        ],
      ),
    );
  }

  sizedBox() {
    return SizedBox(
      height: ScreenUtil().setHeight(20),
      width: ScreenUtil().setWidth(750),
    );
  }

  textSizedBox(BuildContext context, String text) {
    return Container(
      height: ScreenUtil().setHeight(40),
      width: ScreenUtil().setWidth(750),
      color: Theme.of(context).backgroundColor,
      padding: EdgeInsets.only(left: 16.0),
      alignment: Alignment.centerLeft,
      child: Text(
        text,
        style: TextStyle(
          color: Colors.grey,
          fontSize: 12,
        ),
      ),
    );
  }

  inkWell() {
    return Container(
      width: 100,
      height: 50,
      child: InkWell(
        onTap: () {},
        child: Text("水波纹"),
      ),
    );
  }

  appbarText(String text) {
    return Text(text);
  }

  appbarActions(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(50),
      width: ScreenUtil().setWidth(180),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          InkWell(
            child: Icon(
              Icons.search,
              color: Colors.black,
            ),
            onTap: () {
              MyDialogUtil().showAlertDialog(context, "提示", "暂无内容", "确定", "取消");
            },
          ),
          InkWell(
            child: Icon(
              Icons.add_circle_outline,
              color: Colors.black,
            ),
            onTap: () {
              MyDialogUtil().showAlertDialog(context, "提示", "暂无内容", "确定", "取消");
            },
          ),
        ],
      ),
    );
  }

  sigleListTile3(String titleText, String subtitleText) {
    return Container(
      height: ScreenUtil().setHeight(120),
      width: ScreenUtil().setWidth(750),
      decoration: BoxDecoration(
        color: CommonColor().whiteColor(),
        border: Border(bottom: BorderSide(color: Colors.grey[300])),
      ),
      child: ListTile(
        leading: Icon(Icons.person),
        title: Text(titleText),
        subtitle: Text(subtitleText),
        trailing: Text(
          "昨天",
          style: TextStyle(
            color: Colors.grey,
            fontSize: 12,
          ),
        ),
      ),
    );
  }
}
