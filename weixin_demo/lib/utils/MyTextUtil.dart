import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyTextUtil {
  Text text1(String text,
      {Color textColor = Colors.black, double fontSize = 14.0}) {
    return Text(
      text,
      style: TextStyle(color: textColor, fontSize: fontSize),
    );
  }
}
