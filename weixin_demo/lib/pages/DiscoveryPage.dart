import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:weixin_demo/utils/CommonWidget.dart';

class DiscoveryPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<DiscoveryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CommonWidget().appbarText("发现"),
        actions: [
          CommonWidget().appbarActions(context),
        ],
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: Column(
          children: [
            CommonWidget().singleListile(context, "朋友圈", Icons.face, () {}),
            CommonWidget().sizedBox(),
            CommonWidget()
                .singleListile(context, "小程序", Icons.portable_wifi_off, () {}),
          ],
        ),
      ),
    );
  }
}
