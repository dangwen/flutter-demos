import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weixin_demo/utils/CommonWidget.dart';
import 'package:weixin_demo/utils/MyDialogUtil.dart';
import 'package:weixin_demo/utils/MyTextUtil.dart';

class PersonPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<PersonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarHeight: ScreenUtil().setHeight(280),
        elevation: 0,
        actions: [
          Column(
            children: [
              appbarContrainerTop(),
              appbarContrainerBottom(),
            ],
          ),
        ],
      ),
      body: Container(
        width: ScreenUtil().setWidth(750),
        height: ScreenUtil().setHeight(1000),
        padding: EdgeInsets.only(top: ScreenUtil().setHeight(20)),
        color: Theme.of(context).backgroundColor,
        child: Column(
          children: [
            CommonWidget().singleListile(
                context, "支付", Icons.stay_primary_portrait, () {}),
            CommonWidget().sizedBox(),
            CommonWidget().listView(context),
            CommonWidget().sizedBox(),
            CommonWidget().singleListile(context, "设置", Icons.settings, () {}),
          ],
        ),
      ),
    );
  }

  Widget appbarContrainerTop() {
    return Container(
      height: ScreenUtil().setHeight(130),
      width: ScreenUtil().setWidth(750),
      padding: EdgeInsets.only(right: ScreenUtil().setWidth(20)),
      alignment: Alignment.centerRight,
      child: InkWell(
        child: Icon(
          CupertinoIcons.camera,
          color: Colors.black,
        ),
        onTap: () {
          MyDialogUtil().showAlertDialog(context, "提示", "暂无内容", "确定", "取消");
        },
      ),
    );
  }

  Widget appbarContrainerBottom() {
    return Container(
      height: ScreenUtil().setHeight(150),
      width: ScreenUtil().setWidth(750),
      alignment: Alignment.center,
      child: ListTile(
        leading: Container(
          height: ScreenUtil().setHeight(100),
          width: ScreenUtil().setWidth(100),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("lib/images/touxiang1.jpg"),
            ),
          ),
        ),
        title: MyTextUtil().text1("听见风声", fontSize: 20),
        subtitle: MyTextUtil()
            .text1("微信号: fengsheng_1", fontSize: 12, textColor: Colors.grey),
        trailing: Icon(CupertinoIcons.right_chevron),
        onTap: () {
          // MyDialogUtil().showAlertDialog(context, "提示", "暂无内容", "确定", "取消");
        },
      ),
    );
  }
}
