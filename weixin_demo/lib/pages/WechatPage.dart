import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:weixin_demo/utils/CommonWidget.dart';

class WechatPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<WechatPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CommonWidget().appbarText("微信"),
        actions: [
          CommonWidget().appbarActions(context),
        ],
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: ListView(
          children: [
            CommonWidget().sigleListTile3("订阅号消息", "程序人生，年薪超过1500万！苹果CEO库克"),
            CommonWidget().sigleListTile3("中国移动", "月度话费账单提醒"),
            CommonWidget().sigleListTile3("环信all", "环信即时通讯，是世界上最大的通讯系统"),
            CommonWidget().sigleListTile3("宋小伟", "咋回事，怎么又出了bug？"),
            CommonWidget().sigleListTile3("文件传输助手", "[图片]"),
            CommonWidget().sigleListTile3(
                "QQ邮箱提醒", "github: [github] Please veriry your device"),
            CommonWidget().sigleListTile3("wps办公助手", "一招教你成为大师！"),
          ],
        ),
      ),
    );
  }
}
