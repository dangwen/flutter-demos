import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:weixin_demo/utils/CommonWidget.dart';

class ContactListPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<ContactListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CommonWidget().appbarText("通讯录"),
        actions: [
          CommonWidget().appbarActions(context),
        ],
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: Column(
          children: [
            CommonWidget().listView2(context),
            CommonWidget().textSizedBox(context, "我的企业及企业联系人"),
            CommonWidget()
                .singleListile2(context, "huanxin", Icons.ac_unit_sharp, () {}),
            CommonWidget().textSizedBox(context, "A"),
            CommonWidget()
                .singleListile2(context, "aaaaaa", Icons.person, () {}),
            CommonWidget().textSizedBox(context, "B"),
            CommonWidget()
                .singleListile2(context, "bbbb1", Icons.person, () {}),
            CommonWidget()
                .singleListile2(context, "bbbb2", Icons.person, () {}),
            // CommonWidget().textSizedBox(context, "C"),
          ],
        ),
      ),
    );
  }
}
